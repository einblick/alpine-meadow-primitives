# Alpine Meadow Primitives

This repostitory contains the essential libraries for running ML pipelines generated by AutoML.

## Installation

```bash
pip install -r requirements.txt
python setup.py install
```
